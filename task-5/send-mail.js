var http = require("http");
var nodemailer = require('nodemailer');

http.createServer(function (request, response) {
    // Send the HTTP header 
    // HTTP Status: 200 : OK
    // Content Type: text/plain
    response.writeHead(200, { 'Content-Type': 'text/plain' });

    // Send the response body as "Hello World"
    response.end('Hello Node !!!\n');
}).listen(3000);

console.log('Server running at http://127.0.0.1:3000/');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: '...',
        pass: '....'
    },
    port: 3000,
    tls : { rejectUnauthorized: false }
});

var mailOptions = {
    from: '...',
    to: '...',
    subject: 'Sending Email using Node.js',
    text: 'That was easy!'
};

transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
        console.log(error);
    } else {
        console.log('Email sent: ' + info.response);
    }
});